let tileObjects = [
  {
  name: "bcp",
  description: "<p>The Business Card Parser came about as the result of a job interview. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. Part of the job interview involved a programming assignment in which I was given a few hours to create an application that would accept the text from a business card and output the name, email address, and phone number of the individual from the business card. The Business Card Parser demonstrates a strong ability to write complex JavaScript code to solve complicated problems. The Parser recognizes the differences between fax numbers and phone numbers. Additionally, it will find each instance of phone numbers and email addresses, correctly displaying each one for the user. Visually, the program is simple and straightforward, using a dark background and wonderfully contrasting foreground colors. The UI is clean and easy to navigate for the user, even including links to the source code, instructions, and the details of the programming challenge itself.</p>",
  course: "Job Interview"
  
  },
  {
  name: "project3_albums",
  description: "<p>This is the discography page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a music band, with the requirement that the website have at least three pages. The choice of musical group or individual was left up to the student and I chose to build a website for one of my favorite bands: <span class='quote'>Muse</span>. This is the music/discography page, in which the student had to present at least three albums from the musical artist, displaying the tracks appropriately, along with a picture of the album, its title, and the year that it was produced. This page shows good use of space, with good contrast between the background color and the text in the foreground. Alignment is also used to good effect.</p>",
  course: "CMST 385, Principles of Web Design & Technology I"
  
  },
  {
  name: "project3_news",
  description: "<p>This is the news page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a music band, with the requirement that the website have at least three pages. The choice of musical group or individual was left up to the student and I chose to build a website for one of my favorite bands: <span class='quote'>Muse</span>. This is the news page, in which the student was asked to display to the user some recent news of the band. This page allowed me to make good use of padding, as well as color choice, once again going with a darker theme, using a darker background and contrasting that with lighter text up front. The banners used to separate the news items also matches the navigation options at the top of the page, with the news banner text being black to help better distinguish it from the text of the actual news articles.</p>",
  course: "CMST 385, Principles of Web Design & Technology I"
  
  },
  {
  name: "project3",
  description: "<p>This is the home page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a music band, with the requirement that the website have at least three pages. The choice of musical group or individual was left up to the student and I chose to build a website for one of my favorite bands: <span class='quote'>Muse</span>. This is the home page, which very clearly makes it immediately obvious to the user where they have splashed down, leaving no doubt about the purpose of the page. The entire home page makes good use of a background image that grows dynamically with the page and fills the space appropriately. Additionally, the bold header at the top is unmistakable and impossible to miss. The colors used on the page match up very well with the image itself, with just enough contrast being used to distinguish the header across the top of the page, as well as the description of the band given below the header.</p>",
  course: "CMST 385, Principles of Web Design & Technology I"
  
  },
  {
  name: "project4_contact",
  description: "<p>This is the customer review page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a company, with the requirement that the website have at least four pages. The choice of company type was left up to the student and I chose to build a website for a hypothetical pool installation company called <span class='quote'>Willie’s Builders</span>. This is the contact page, where the customer is given several options for contacting the company, depending on their specific needs. While there is nothing terribly complicated about this page, it shows a good use of color to evoke the feeling of summer, and sometimes simple is better and less is more. That’s what this page shows. </p>",
  course: "CMST 385, Principles of Web Design & Technology I"
  
  },
  {
  name: "project4_gallery",
  description: "<p>This is the gallery page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a company, with the requirement that the website have at least four pages. The choice of company type was left up to the student and I chose to build a website for a hypothetical pool installation company called <span class='quote'>Willie’s Builders</span>. This is the gallery page from that website, and the point of this page was to display a table of data to users that clearly showed the results of previous work that <span class='quote'>Willie’s Builders</span> had completed. The site uses colors that evoke the feeling of summer, which matches the theme of the company. The table of data also does a good job of showing slight contrast between rows, making it easier for the eye to scan information horizontally without accidentally changing rows. The table also makes good use of space and padding, with the columns clearly defined without forcing the use of unnecessary solid separation lines.</p>",
  course: "CMST 385, Principles of Web Design & Technology I"
  
  },
  {
  name: "project4_home",
  description: "<p>This is the home page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a company, with the requirement that the website have at least four pages. The choice of company type was left up to the student and I chose to build a website for a hypothetical pool installation company called <span class='quote'>Willie’s Builders</span>. The first thing you might notice is the use of color on the site. The colors that were chosen were selected because of the theme of summer living, with blue, orange, and yellow all evoking the sense of summer. This home page also makes good use of simplistic text using a slightly different background shade to differentiate it from the rest of the page and to ensure that there is just enough contrast that it isn’t lost to the user. The background image makes it very clear to the user from the very beginning what this company is all about.</p>",
  course: "CMST 385, Principles of Web Design & Technology I"

  },
  {
  name: "project4_reviews",
  description: "<p>This is the customer review page for a larger project that I had to complete in CMST 385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. The project involved building and hosting a website for a company, with the requirement that the website have at least four pages. The choice of company type was left up to the student and I chose to build a website for a hypothetical pool installation company called <span class='quote'>Willie’s Builders</span>. This is the reviews page, where I had the opportunity to show proper use of font styles. The quotes are italicized and appropriately sized, while the names of the authors are clearly defined, using different font styles, and spaced and positioned appropriately, as well.</p>",
  course: "CMST 385, Principles of Web Design & Technology I"
  },
  {
  description: "<p> The goal of this project was to create and host our first website. This was the first assignment from CMST385, from UMGC. I built the page using JavaScript, HTML, and CSS, and hosted it on GitLab, using Gitlab Pages to serve the static site. Traditionally, when a programmer is learning a new language, the first thing they will typically do is create some program that prints or displays a 'Hello, World' message. And that is what I did in this project. Having a strong preference for dark themes, I decided to go with a darker background, which I think contrasts very nicely with the font colors of light purple and white. The focal point is the “Hello, World” message, displayed prominently at the top of the screen, with a quote from my favorite author (Steven Erikson) underneath that reads: <span class='quote'> “Don’t worry. I am like most people. I can keep my eyes and still see nothing.”</span> I believe that this project, though quite simple, shows good use of contrasting colors, as well as proper spacing and placement of the few page elements that are present.</p>",
  name: "project1",
  course: "CMST 385, Principles of Web Design & Technology I"
  }
];

const fillTemplate = imageObject => {
  return (
     `<div class="imageHolder" description=${imageObject.description} name=${imageObject.name}>
        <div class="imageOverlay">
          <div class="textHolder">${imageObject.name}:  ${imageObject.subtext}</div>
          <img class="image" src="./images/project1.jpg">       
        </div>
      </div>`
  );
};

const handleClick = evt => {
  var el = evt.target;
  if (el.tagName === "P") el = el.parentElement;
  var id = el.id;
  console.log(id);
  var tile = tileObjects.find(o => o.name === id);
  var description = tile.description;
  var course = tile.course;
  var imgSrc = el.nextElementSibling.attributes.src.value;
  var title = el.innerText;
  [...document.getElementsByClassName("textHolder")].forEach(el => el.style.display = "none");
  showBackdrop(title, description, course, imgSrc);
};

const handleBackdropClick = (evt) => {
  evt.stopPropagation();
  var backdrop = document.getElementById("backdrop");
  if (backdrop.style.display === "grid"){
    closeBackdrop();
  }
  else return;
}

const showBackdrop = (title, description, course, imgSrc) => {
  document.getElementById("tileTitle").textContent = title;
  document.getElementById("actualDescription").innerHTML = description;
  document.getElementById("tileClass").textContent = course;
  document.getElementById("tileImage").src = imgSrc;
  document.getElementById("backdrop").style.display = "grid";
  window.setTimeout( () => {
    document.getElementById("tileDisplay").classList.remove("hideTile");
  },42);

}

const closeBackdrop = (evt) => {
  window.setTimeout( () => {
    document.getElementById("backdrop").style.display = "none";
  },600);
  document.getElementById("tileDisplay").classList.add("hideTile");
}

const showHomeParagraphs = () => {
  window.setTimeout( () => {
    [...document.getElementsByClassName("homeParagraph")].forEach(p => p.classList.add("visibleText"));
  }, 500);
};

const hideHomeParagraphs = () => {
  [...document.getElementsByClassName("homeParagraph")].forEach(p => p.classList.remove("visibleText"));
};

const showHome = evt => {
  document.body.style.overflow = "hidden";
  var highlighted = document.getElementsByClassName("highlighted")[0].id;
  if (highlighted === "homeButton") return;
  [...document.getElementsByClassName("highlighted")].forEach(el => el.classList.remove("highlighted"));
  evt.target.classList.add("highlighted");
  document.getElementById("galleryHolder").classList.add("hideTile");
  if (highlighted === "contactFormButton"){
    document.getElementById("contactForm").classList.add("hidden");
    document.getElementById("home").classList.remove("hidden");
    showHomeParagraphs();
    document.body.style.overflow = "auto";
  }
  else {
    window.setTimeout( () => {
      document.getElementById("galleryHolder").classList.add("hidden");
      document.getElementById("home").classList.remove("hidden");
      showHomeParagraphs();
      document.body.style.overflow = "auto";
    }, 800)
  }
};

const showGallery = evt => {
  document.body.style.overflow = "hidden";
  var highlighted = document.getElementsByClassName("highlighted")[0].id;
  if (highlighted === "galleryButton") return;
  [...document.getElementsByClassName("highlighted")].forEach(el => el.classList.remove("highlighted"));
  evt.target.classList.add("highlighted");
  if (highlighted === "contactFormButton"){
    document.getElementById("galleryHolder").classList.remove("hidden");
    window.setTimeout( () => {
      document.getElementById("contactForm").classList.add("hidden");
      document.getElementById("galleryHolder").classList.remove("hideTile");
      window.setTimeout(() => {
        document.body.style.overflow = "auto";
      },1000)
    },500);
    
  }
  else {
    hideHomeParagraphs();
    document.getElementById("galleryHolder").classList.remove("hidden");
    window.setTimeout( () => {
      document.getElementById("home").classList.add("hidden");
      document.getElementById("galleryHolder").classList.remove("hideTile");
      window.setTimeout(() => {
        document.body.style.overflow = "auto";
      },1000)
    },800)

  }
  
};

const showContact = evt => {
  [...document.getElementsByClassName("form-control")].forEach(el => el.value = "");
  document.body.style.overflow = "hidden";
  var highlighted = document.getElementsByClassName("highlighted")[0].id;
  if (highlighted === "contactFormButton") return;
  [...document.getElementsByClassName("highlighted")].forEach(el => el.classList.remove("highlighted"));
  evt.target.classList.add("highlighted");
  if (highlighted === "galleryButton"){
    document.getElementById("galleryHolder").classList.add("hideTile");
    window.setTimeout( () => {
      document.getElementById("galleryHolder").classList.add("hidden");
      document.getElementById("contactForm").classList.remove("hidden");
      document.body.style.overflow = "auto";
    }, 800)
  }
  else if (highlighted === "homeButton"){
    hideHomeParagraphs();
    window.setTimeout( () => {
      document.getElementById("home").classList.add("hidden");
      document.getElementById("contactForm").classList.remove("hidden");
      document.body.style.overflow = "auto";
    },800);
  }
};

const showText = evt => evt.target.firstElementChild.style.display = "grid";

const hideText = evt => evt.target.firstElementChild.style.display = "none";

const init = () => {
  [...document.getElementsByClassName("imageHolder")].forEach(div => div.addEventListener("click", handleClick));
  document.getElementById("homeButton").addEventListener("click", showHome);
  document.getElementById("galleryButton").addEventListener("click", showGallery);
  document.getElementById("contactFormButton").addEventListener("click", showContact);
  [...document.getElementsByClassName("imageOverlay")].forEach(el => el.addEventListener("mouseenter", showText));
  [...document.getElementsByClassName("imageOverlay")].forEach(el => el.addEventListener("mouseleave", hideText));
  document.getElementById("closeButton").addEventListener("click", closeBackdrop);
  document.getElementById("backdrop").addEventListener("click", handleBackdropClick);
  [...document.getElementsByClassName("form-control")].forEach(el => el.value = "");
  showHomeParagraphs();
};

init();
